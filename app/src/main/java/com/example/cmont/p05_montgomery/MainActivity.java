package com.example.cmont.p05_montgomery;

import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    //Random number generator
    private Random randomGenerator = new Random();
    //Current picture
    private int random_image;

    //x1 and x2 to track left to right
    private float x1, x2;
    //min distance to consider a swipe
    static final int MIN_DISTANCE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setImage();

    }

    public Runnable setImage()
    {
        random_image = randomGenerator.nextInt(3);

        //Get the image view
        ImageView im = (ImageView) findViewById(R.id.imageView);

        //Change image
        if(random_image == 0)
        {
            im.setImageResource(R.drawable.apple);
        }
        else if (random_image == 1)
        {
            im.setImageResource(R.drawable.bananna);
        }
        else
        {
            im.setImageResource(R.drawable.orange);
        }
        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                float deltaX2 = x1 - x2;

                System.out.println(x1 + " " + x2);

                if (Math.abs(deltaX) > MIN_DISTANCE || Math.abs(deltaX2) > MIN_DISTANCE)
                {

                    //Get the image view
                    ImageView im = (ImageView) findViewById(R.id.imageView);

                    //Change image
                    if(random_image == 0)
                    {
                        im.setImageResource(R.drawable.apple_cut);
                    }
                    else if (random_image == 1)
                    {
                        im.setImageResource(R.drawable.bananna_cut);
                    }
                    else
                    {
                        im.setImageResource(R.drawable.orange_cut);
                    }


                    Toast.makeText(this, "Do it again!", Toast.LENGTH_SHORT).show();

                    //Create a delay handler
                    Handler delayHandler= new Handler();
                    Runnable r=new Runnable()
                    {
                        @Override
                        public void run() {

                            // Call this method after 1000 milliseconds
                            setImage();

                        }

                    };
                    delayHandler.postDelayed(r, 2000);
                }
                else
                {
                    Toast.makeText(this, "Slice left to right or right to left!", Toast.LENGTH_SHORT).show ();
                }
                break;
        }
        return super.onTouchEvent(event);
    }
}
